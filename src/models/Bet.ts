import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./User";
import { Round } from "./Round";

@Entity()
export class Bet {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("decimal")
    amount: number;

    @Column("decimal", { nullable: true })
    cashedOutAtMultiplier: number;

    @Column("decimal")
    cashedOutAmount: number;

    @Column()
    status: string;

    @ManyToOne(() => User, (user) => user.bets, { cascade: true })
    user: User;

    @ManyToOne(() => Round, (round) => round.bets)
    round: Round;
}
