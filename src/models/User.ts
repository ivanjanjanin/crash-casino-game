import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Bet } from "./Bet";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique: true})
    username: string;

    @OneToMany(() => Bet, (bet) => bet.user)
    bets: Bet[];
}
