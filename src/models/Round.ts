import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Bet } from "./Bet";
import { GameStateEnum } from "../enums/Game";

@Entity()
export class Round {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("timestamp")
    startedAt: Date;

    @Column("timestamp", { nullable: true })
    endedAt: Date;

    @Column("decimal")
    maxMultiplier: number;

    @Column("decimal")
    betsWon: number;

    @Column("decimal")
    betsLost: number;

    @Column("decimal")
    amountBetted: number;

    @Column("decimal")
    amountCashedOut: number;

    @Column("decimal")
    amountLost: number;

    @Column({
        type: "enum",
        enum: GameStateEnum
    })
    currentState: GameStateEnum;

    @OneToMany(() => Bet, (bet) => bet.round, { cascade: ["insert", "update"] }) 
    bets: Bet[];
}
