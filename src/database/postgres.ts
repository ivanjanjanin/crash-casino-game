import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "../models/User"
import { Bet } from "../models/Bet"
import { Round } from "../models/Round"
import { POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_NAME } from "../config"

export const AppDataSource = new DataSource({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: POSTGRES_USER,
    password: POSTGRES_PASSWORD,
    database: POSTGRES_NAME,
    synchronize: true,
    logging: false,
    entities: [User, Round, Bet],
    migrations: [],
    subscribers: [],
})
