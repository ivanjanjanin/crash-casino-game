import { parentPort } from 'worker_threads';
import { createServer } from 'http';
import { WebSocket } from './websocket';
import { Logger } from './logger';
import { WS_PORT } from '../config';

const httpServer = createServer();
const wsHandler = WebSocket.getInstance(httpServer);
const logger = Logger.create('wsWorker');

wsHandler.init();

httpServer.listen(WS_PORT, () => {
  logger.info(`WebSocket server listening on port ${WS_PORT}`);
});

parentPort?.on('message', (message) => {
  switch (message.type) {
    case 'gameUpdate':
      wsHandler.emitMessage('gameUpdate', message.data);
      break;
    case 'betPlaced':
      wsHandler.emitMessage('betPlaced', message.data);
      break;
    case 'cashOut':
      wsHandler.emitMessage('betCashedOut', message.data);
      break;
  }
});