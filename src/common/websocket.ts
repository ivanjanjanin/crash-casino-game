import { Server as HTTPServer } from 'http';
import { Server, Socket } from 'socket.io';
import { Logger } from './logger';
import { GameState } from '../types/GameState';

const logger = Logger.create('WebSocket');

export class WebSocket {
  private static instance: WebSocket;
  private io: Server;
  private gameState: GameState;

  private constructor(server: HTTPServer) {
    if (!server) {
      throw new Error("Server not set. Exiting.");
    }
    this.io = new Server(server, {
      cors: {
        origin: "*",
        methods: ["GET", "POST"]
      }
    });
    this.gameState = {
      multiplier: 1,
      state: 'waiting',
      bets: []
    };
  }

  public static getInstance(server: HTTPServer): WebSocket {
    if (!WebSocket.instance) {
      WebSocket.instance = new WebSocket(server);
    }
    return WebSocket.instance;
  }

  public init() {
    this.io.on('connection', (socket: Socket) => {
      this.handleConnection(socket);
    });
  }

  private handleConnection(socket: Socket) {
    logger.info('A user connected', socket.id);

    socket.emit('gameUpdate', this.gameState);

    socket.on('disconnect', () => {
      this.handleDisconnect(socket);
    });
  }

  private handleDisconnect(socket: Socket) {
    logger.info('User disconnected', socket.id);
  }

  public emitMessage(event: string, message: any) {
    this.io.emit(event, message);
  }
}
