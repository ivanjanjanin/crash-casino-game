import { Worker } from 'worker_threads';
import path from 'path';

export class WorkerSingleton {
  private static instance: Worker;

  private constructor() { }

  public static getInstance(): Worker {
    if (!WorkerSingleton.instance) {
      const workerPath = path.resolve(__dirname, '../common/wsWorker.js');
      WorkerSingleton.instance = new Worker(workerPath);
    }
    return WorkerSingleton.instance;
  }
}