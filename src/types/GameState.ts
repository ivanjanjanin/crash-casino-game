export interface GameState {
    multiplier: number;
    state: string;
    bets: any[];
}