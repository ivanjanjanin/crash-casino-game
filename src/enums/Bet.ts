export enum BetStatusEnum {
    PLACED = "Placed",
    WON = "Won",
    LOST = "Lost",
}