export enum GameStateEnum {
    ACCEPTING_BETS = "Accepting bets",
    PREPARING = "Preparing",
    FLYING = "Flying",
    CRASH = "Crash"
}