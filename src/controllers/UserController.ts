import { AppDataSource } from "../database/postgres";
import { NextFunction, Request, Response } from "express";
import { User } from "../models/User";
import { Logger } from "../common/logger";

export class UserController {
    private userRepository = AppDataSource.getRepository(User);
    private logger = Logger.create('UserController');

    async all(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
        try {
            const users = await this.userRepository.find({ relations: ["bets"] });
            return response.json(users);
        } catch (error) {
            next(error);
         }
    }

    async one(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
        const id = parseInt(request.params.id);
        try {
            const user = await this.userRepository.findOne({
                where: { id },
                relations: ["bets"]
            });

            if (!user) {
                return response.status(404).json({ message: "User not found" });
            }
            return response.json(user);
        } catch (error) {
            next(error);
        }
    }

    async save(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
        const { username } = request.body;

        const trimmedUsername = username?.trim();
        if (!trimmedUsername) {
            return response.status(400).json({ message: "Username was not provided or is empty" });
        }
        if (trimmedUsername.length < 3 || trimmedUsername.length > 20) {
            return response.status(400).json({ message: "Username must be between 3 and 20 characters long" });
        }
        const usernameRegex = /^[a-zA-Z0-9_]+$/;
        if (!usernameRegex.test(trimmedUsername)) {
            return response.status(400).json({ message: "Username can only contain letters, numbers, and underscores" });
        }

        try {
            const user = new User();
            user.username = username;

            const savedUser = await this.userRepository.save(user);
            return response.status(201).send(savedUser);
        } catch (error: any) {
            this.logger.error(`Error occurred during creation of new user: ${error.message ?? error}`);
            
            if (error.code === '23505') { // Violation error code for PostgreSQL
                return response.status(409).json({ message: "Username already exists" });
            }
            next(error)
        }
    }

    async remove(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
        const id = parseInt(request.params.id);
        try {
            let userToRemove = await this.userRepository.findOneBy({ id });

            if (!userToRemove) {
                return response.status(404).json({ message: "User not found" });
            }

            await this.userRepository.remove(userToRemove);

            return response.json({ message: "User has been removed" });
        } catch (error) {
            next(error);
        }
    }

    async getUserBets(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
        const userId = parseInt(request.params.id);
        try {
            const user = await this.userRepository.findOne({
                where: { id: userId },
                relations: ["bets"]
            });

            if (!user) {
                return response.status(404).json({ message: "User not found" });
            }

            return response.json(user.bets);
        } catch (error) {
            next(error);
        }
    }
}
