import { Request, Response, NextFunction } from 'express';
import { AppDataSource } from "../database/postgres";
import { Bet } from "../models/Bet";
import { User } from "../models/User";
import { Round } from "../models/Round";
import { GameStateEnum } from "../enums/Game";
import { BetStatusEnum } from "../enums/Bet";
import { Logger } from "../common/logger";
import { GameService } from '../services/gameService';
import { WorkerSingleton } from '../common/workerSingleton';


export class BetController {
  private gameService = GameService.getInstance();
  private logger = Logger.create('BetController');
  private wsWorker = WorkerSingleton.getInstance();

  async placeBet(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
    const { amount, cashOutAtMultiplier, userId, roundId } = request.body;

    if (!amount || amount <= 0) {
      return response.status(400).json({ message: "Invalid bet amount" });
    }
    if (cashOutAtMultiplier && cashOutAtMultiplier <= 1) {
      return response.status(400).json({ message: "Invalid cash out multiplier" });
    }

    const queryRunner = AppDataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const user = await queryRunner.manager.findOne(User, { where: { id: userId } });
      const round = await queryRunner.manager.findOne(Round, { where: { id: roundId } });

      if (!user) {
        await queryRunner.rollbackTransaction();
        return response.status(404).json({ message: "User not found" });
      }

      if (!round) {
        await queryRunner.rollbackTransaction();
        return response.status(404).json({ message: "Round not found" });
      }

      if (round.currentState !== GameStateEnum.ACCEPTING_BETS) {
        await queryRunner.rollbackTransaction();
        return response.status(400).json({ message: "Round betting phase passed. Please, wait for next round." });
      }

     let bet = await queryRunner.manager.findOne(Bet, {
      where: { user: { id: userId }, round: { id: roundId } }
    });

    if (bet) {
      bet.amount = amount;
    } else {
      bet = new Bet();
      bet.amount = amount;
      bet.cashedOutAmount = 0;
      bet.status = BetStatusEnum.PLACED;
      bet.user = user;
      bet.round = round;
    }

      const savedBet = await queryRunner.manager.save(Bet, bet);

      await queryRunner.commitTransaction();

      const eventToEmit = {
        amount : savedBet.amount,
        status : savedBet.status
      }

      this.wsWorker.postMessage({ type: 'betPlaced', data: eventToEmit });
      
      return response.status(201).json({ message: "Bet placed successfully", data: eventToEmit });
    } catch (error: any) {
      await queryRunner.rollbackTransaction();
      this.logger.error(`Error occurred during placing bet: ${error.message ?? error}`);
      next(error);
    } finally {
      await queryRunner.release();
    }
  }

  async cashOutBet(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
    const { betId } = request.body;

    const queryRunner = AppDataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const bet = await queryRunner.manager.findOne(Bet, { where: { id: betId }, relations: ["round"] });

      if (!bet) {
        await queryRunner.rollbackTransaction();
        return response.status(404).json({ message: "Bet not found" });
      }

      if (bet.status !== BetStatusEnum.PLACED) {
        await queryRunner.rollbackTransaction();
        return response.status(400).json({ message: "Bet cannot be cashed out, it might be unexistent or cashed out already" });
      }

      if(bet.round.currentState !== GameStateEnum.FLYING ){
        return response.status(400).json({ message: "Not possible to cash out now, game is not active" });
      }

      const currentMultiplier = this.gameService.getCurrentMultiplier();
      bet.cashedOutAmount = bet.amount * currentMultiplier;
      bet.cashedOutAtMultiplier = currentMultiplier;
      bet.status = BetStatusEnum.WON;

      const savedBet = await queryRunner.manager.save(Bet, bet);

      await queryRunner.commitTransaction();

      const eventToEmit = {
        cashedOutAmount : savedBet.cashedOutAmount,
        cashedOutAtMultiplier : savedBet.cashedOutAtMultiplier
      }

      this.wsWorker.postMessage({ type: 'cashOut', data: eventToEmit });

      return response.status(200).json({ message: "Successfully cashed out bet", data: eventToEmit });
    } catch (error: any) {
      await queryRunner.rollbackTransaction();
      this.logger.error(`Error occurred during cashing out bet: ${error.message ?? error}`);
      next(error)
    } finally {
      await queryRunner.release();
    }
  }
}
