import { Request, Response, NextFunction } from 'express';
import { AppDataSource } from "../database/postgres";
import { Round } from "../models/Round";
import { Logger } from "../common/logger";
import { GameStateEnum } from "../enums/Game";

export class RoundController {
  private roundRepository = AppDataSource.getRepository(Round);
  private logger = Logger.create('RoundController');

  async getRoundHistory(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
    try {
      const rounds = await this.roundRepository.find({
        where: { currentState: GameStateEnum.CRASH },
        select: ["id", "startedAt", "endedAt", "maxMultiplier"],
        order: { startedAt: "DESC" }
      });
      return response.status(200).json(rounds);
    } catch (error: any) {
      this.logger.error(`Error fetching round history: ${error.message ?? error}`);
      next(error)
    }
  }
}
