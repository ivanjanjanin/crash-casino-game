import express, { Application, Request, Response, NextFunction } from 'express';
import * as bodyParser from 'body-parser';
import { AppDataSource } from "./database/postgres";
import { Routes } from "./routes/index";
import { PORT } from "./config";
import { Logger } from "./common/logger";
import { GameService } from "./services/gameService";
import { RoundService } from "./services/roundService";
import { WorkerSingleton } from './common/workerSingleton';

class App {
  private app: Application;
  private logger = Logger.create('MainLogger');
  private roundService = RoundService.getInstance();
  private gameService = GameService.getInstance();
  private wsWorker = WorkerSingleton.getInstance();

  constructor() {
    this.app = express();
    this.configureMiddleware();
    this.setupRoutes();
    this.setupErrorHandling();
    this.logger.info('App initialized');
  }

  private configureMiddleware(): void {
    this.app.use(bodyParser.json());
  }

  private setupRoutes(): void {
    Routes.forEach(route => {
      (this.app as any)[route.method](route.route, async (req: Request, res: Response, next: NextFunction) => {
        try {
          const result = await (new (route.controller as any))[route.action](req, res, next);
          if (result !== null && result !== undefined && !res.headersSent) {
            res.json(result);
          }
        } catch (error) {
          next(error);
        }
      });
    });
  }

  private setupErrorHandling(): void {
    this.app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      this.logger.error(`Error occurred: ${err.message ?? err}`);
      if (!res.headersSent) {
        res.status(500).json({ message: "Internal server error" });
      } else {
        next(err);
      }
    });
  }

  private async startNewRound(): Promise<void> {
    try {
      await this.gameService.startNewRound();
      this.logger.info('New round started');
    } catch (error: any) {
      this.logger.error(`Error starting new round: ${error.message}`);
    }
  }

  private async updateGameState(): Promise<void> {
    while (true) {
      try {
        const latestRound = await this.roundService.getLatestRound();

        if (latestRound) {
          const gameState = {
            multiplier: this.gameService.getCurrentMultiplier(),
            state: latestRound.currentState,
            bets: latestRound.bets
          };

          this.wsWorker.postMessage({ type: 'gameUpdate', data: gameState });
        } else {
          this.logger.info("No round data found.");
        }

        await new Promise(resolve => setTimeout(resolve, 100)); // Update every 100ms
      } catch (error: any) {
        this.logger.error(`Error in game loop: ${error.message ?? error}`);
      }
    }
  }

  public async initialize(): Promise<void> {
    try {
      await AppDataSource.initialize();
      this.logger.info('Database initialized');
  
      this.app.listen(PORT, () => {
        this.logger.info(`Express server has started on port ${PORT}`);
      });

      this.updateGameState().catch(error => {
        this.logger.error(`Error in game loop: ${error.message}`);
      });
  
      await this.startNewRound();
  
    } catch (error: any) {
      this.logger.error(`Initialization error: ${error.message}`);
    }
  }
}

const app = new App();
app.initialize().catch(error => console.log(error));
