import { RoundService } from "../services/roundService";
import { Round } from "../models/Round";

export class GameService {
    private static instance: GameService;
    private roundService: RoundService;
    public gameDurationInSec: number;

    private constructor() {
        this.roundService = RoundService.getInstance();
        this.gameDurationInSec = 0;
    }

    public static getInstance(): GameService {
        if (!GameService.instance) {
            GameService.instance = new GameService();
        }
        return GameService.instance;
    }

    private static getCurrentMultiplier = (gameDurationInSec: number): number => {
        return 1 + 0.05 * gameDurationInSec + 0.005 * Math.pow(gameDurationInSec, 2) + 0.00000000001 * Math.pow(gameDurationInSec, 7.2);
    }

    public getCurrentMultiplier(): number {
        return GameService.getCurrentMultiplier(this.gameDurationInSec);
    }

    public startAcceptingBets = async (): Promise<Round> => {
        const round = await this.roundService.createNewRound();

        await new Promise<void>((resolve) => {
            setTimeout(() => {
                resolve();
            }, 10000); // 10 seconds
        });

        return round;
    }

    public startPreparingPeriod = async (round: Round): Promise<void> => {
        await this.roundService.startPreparingPeriod(round);
        await new Promise<void>((resolve) => {
            setTimeout(() => {
                resolve();
            }, 2000); // 2 seconds
        });
    }

    public startFlyingPeriod = async (round: Round): Promise<void> => {
        await this.roundService.startFlyingPeriod(round);

        const maxMultiplier = round.maxMultiplier;
        this.gameDurationInSec = 0;

        await new Promise<void>((resolve) => {
            const interval = setInterval(() => {
                const currentMultiplier = this.getCurrentMultiplier();

                if (currentMultiplier >= maxMultiplier) {
                    clearInterval(interval);
                    resolve();
                }

                this.gameDurationInSec += 0.1; // increment by 0.1 seconds for each 100ms
            }, 100); // updates every 100 ms
        });
    }

    public crashTheGame = async (round: Round): Promise<void> => {
        await this.roundService.finishRound(round);
        await new Promise<void>((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1000); // 1 second
        });
    }

    public startNewRound = async (): Promise<void> => {
        while (true) {
            const round = await this.startAcceptingBets();

            await this.startPreparingPeriod(round);

            await this.startFlyingPeriod(round);

            await this.crashTheGame(round);

        }
    }
}
