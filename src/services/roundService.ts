import { AppDataSource } from "../database/postgres";
import { Round } from "../models/Round";
import { GameStateEnum } from "../enums/Game";
import { Logger } from "../common/logger";
import { BetStatusEnum } from "../enums/Bet";
import { Bet } from "../models/Bet";

export class RoundService {
    private static instance: RoundService;
    private logger = Logger.create('RoundService');
    private roundRepository = AppDataSource.getRepository(Round);

    private constructor() {}

    public static getInstance(): RoundService {
        if (!RoundService.instance) {
            RoundService.instance = new RoundService();
        }
        return RoundService.instance;
    }

    private static getMaxMultiplier = (): number => {
        return Math.floor(Math.random() * 1_000) / 100 + 1;
    }

    public async createNewRound(): Promise<Round> {
        try {
            const round = new Round();
            round.startedAt = new Date();
            round.maxMultiplier = RoundService.getMaxMultiplier();
            round.betsWon = 0;
            round.betsLost = 0;
            round.amountBetted = 0;
            round.amountCashedOut = 0;
            round.amountLost = 0;
            round.currentState = GameStateEnum.ACCEPTING_BETS;
            
            const savedRound = await this.roundRepository.save(round);
            this.logger.info(`New round started with ID: ${round.id}`);
            return savedRound;
        } catch (error: any) {
            this.logger.error(`Error occurred during starting new round: ${error.message ?? error}`);
            throw new Error("Internal server error");
        }
    }

    public async startPreparingPeriod(round: Round): Promise<Round> {
        try {
            round.currentState = GameStateEnum.PREPARING;
            const savedRound = await this.roundRepository.save(round);
            this.logger.info(`Round ID ${round.id} entered PREPARING period.`);
            return savedRound;
        } catch (error: any) {
            this.logger.error(`Error occurred during initialization of preparing period: ${error.message ?? error}`);
            throw new Error("Internal server error");
        }
    }

    public async startFlyingPeriod(round: Round): Promise<Round> {
        try {
            round.currentState = GameStateEnum.FLYING;
            const savedRound = await this.roundRepository.save(round);
            this.logger.info(`Round ID ${round.id} entered FLYING period with max multiplier ${round.maxMultiplier}.`);
            return savedRound;
        } catch (error: any) {
            this.logger.error(`Error occurred during initialization of flying period: ${error.message ?? error}`);
            throw new Error("Internal server error");
        }
    }

    public async finishRound(round: Round): Promise<Round> {
        try {
            const roundWithBets = await this.roundRepository.findOne({
                where: { id: round.id },
                relations: ["bets"]
            });
    
            if (!roundWithBets) {
                throw new Error(`Round with ID ${round.id} not found`);
            }
    
            const bets = roundWithBets.bets;
    
            for (const bet of bets) {
                if (bet.status === BetStatusEnum.PLACED) {
                    bet.status = BetStatusEnum.LOST;
                }
            }
    
            round.betsWon = bets.filter(bet => bet.status === BetStatusEnum.WON).length;
            round.betsLost = bets.filter(bet => bet.status === BetStatusEnum.LOST).length;
            round.amountBetted = bets.reduce((total, bet) => total + bet.amount, 0);
            round.amountCashedOut = bets.filter(bet => bet.status === BetStatusEnum.WON).reduce((total, bet) => total + bet.cashedOutAmount, 0);
            round.amountLost = bets.filter(bet => bet.status === BetStatusEnum.LOST).reduce((total, bet) => total + bet.amount, 0);
            round.currentState = GameStateEnum.CRASH;
            round.endedAt = new Date();
    
            const betRepository = AppDataSource.getRepository(Bet);
            await betRepository.save(bets);
    
            const savedRound = await this.roundRepository.save(round);
            this.logger.info(`Round ID ${round.id} has finished.`);
            return savedRound;
        } catch (error: any) {
            this.logger.error(`Error occurred during finishing the round: ${error.message ?? error}`);
            throw new Error("Internal server error");
        }
    }

    public async getLatestRound(): Promise<Round | null> {
        try {
            const [latestRound] = await this.roundRepository.find({
                order: { startedAt: "DESC" },
                relations: ["bets"],
                take: 1
            });
            return latestRound || null;
        } catch (error: any) {
            this.logger.error(`Error occurred during fetching the latest round: ${error.message ?? error}`);
            throw new Error("Internal server error");
        }
    }
}