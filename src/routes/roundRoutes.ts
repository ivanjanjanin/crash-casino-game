import { RoundController } from "../controllers/RoundController";

export const roundRoutes = [
  {
    method: "get",
    route: "/rounds/history",
    controller: RoundController,
    action: "getRoundHistory"
  },
];
