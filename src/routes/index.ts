import { betRoutes } from "./betRoutes";
import { roundRoutes } from "./roundRoutes";
import { userRoutes } from "./userRoutes";

export const Routes = [
    ...userRoutes,
    ...betRoutes,
    ...roundRoutes
]