import { BetController } from "../controllers/BetController";

export const betRoutes = [{
    method: "post",
    route: "/bets",
    controller: BetController,
    action: "placeBet"
}, {
    method: "post",
    route: "/bets/cashout",
    controller: BetController,
    action: "cashOutBet"
}];
