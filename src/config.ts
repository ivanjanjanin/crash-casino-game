import path from 'path';
import fs from 'fs';

const packageInfo = JSON.parse(
  fs.readFileSync(`${path.resolve()}${path.sep}package.json`).toString(),
);

export const PORT = process.env.PORT || 3000;
export const WS_PORT = process.env.WS_PORT || 3001;


export const APP_NAME = `${packageInfo.name}`;
export const LOG_LEVEL = process.env.LOG_LEVEL || 'info';
export const LOG_PATH = process.env.LOG_PATH === 'false' ? undefined : process.env.LOG_PATH || './logs';


export const POSTGRES_USER = process.env.POSTGRES_USER || 'mochalabs'
export const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD || 'mochalabs_secret'
export const POSTGRES_NAME = process.env.POSTGRES_NAME || 'backend_task'
