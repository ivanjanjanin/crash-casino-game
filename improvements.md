Improvements for the current version:

    Improvement number 1 -> Implementation of the 404 not found middleware to return upon route which doesn't exist.

    Improvement number 2 -> Env to manage secrets and sensitive project info.

    Improvement number 3 -> Implement index on the round since we are fetching the history of the rounds using startedAt field.

    Improvement number 4 -> User registration with web3 wallet, display of user balances by real crypto methods.

    Improvement number 5 -> Authorization and authentication to the routes so that only known users could access them.

    Improvement number 6 -> Hot wallet topped up with balances which user interacts with?

    Improvement number 7 -> Generating rounds beforehand to know exact start and end times so that managing of crashed rounds could be better handled.

    Improvement number 8 -> Additional worker to manage state of the game so that the main thread can be free for http requests.

    Improvement number 9 -> Unit tests for the whole app to ensure each part is working correctly. 

    Improvement number 10 -> Pass through all returned data and format it nicer so that it corresponds to a certain standard.

Improvements for the next version of game:

    Improvement number 1 -> Implementing a different design pattern (factory or similar) to allow extension of the game for a multiple different crash games to be ran concurrently maybe at different routes as well..

    