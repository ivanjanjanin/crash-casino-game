# Notes for Starting the App

## Prerequisites

Ensure you have the following installed on your system:
- Docker
- Docker Compose
- Node.js and npm

## Step-by-Step Instructions

### 1. Set Up and Start the Database

Use Docker Compose to set up and start the PostgreSQL database. Run the following command in the root directory of the project:

```sh
docker-compose up -d
```

This will start the PostgreSQL container defined in the `docker-compose.yml` file in a detached mode.

### 2. Install Dependencies

Install the required npm dependencies by running:

```sh
npm install
```

### 3. Start the Application

Start the application using the following command:

```sh
npm start
```

This will start the Express server, initialize the database, set up WebSocket connections, and begin the game loop.

### 4. Access the Application

Once the application is running, you can interact with it by sending requests to:

```
http://localhost:3000
```

## Available Routes

### POST `/users`

Create a new user.

**Request Body:**

```json
{
  "username": "ivan"
}
```

### POST `/bets`

Create a new bet.

**Request Body:**

```json
{
  "amount": 100,
  "userId": 1,
  "roundId": 2
}
```

### POST `/bets/cashout`

Cash out a bet.

**Request Body:**

```json
{
  "betId": 1
}
```

### GET `/users/:id/bets`

Retrieve all bets for a specific user.

Replace `:id` with the actual user ID.

**Example:**

```
http://localhost:3000/users/1/bets
```

### GET `/rounds/history`

Retrieve all rounds history with corresponding multipliers.

## Summary of Commands

```sh
# Set up and start the database using Docker Compose
docker-compose up -d

# Install npm dependencies
npm install

# Start the application
npm start
```

By following these instructions, you should be able to successfully start up the app and begin interacting with it.